﻿
public enum CarType {
    Car0,
    Car1,
    Car2,
    Car3,
    Car4,
    None
}

public enum ColorType {
    White,
    Red,
    Green,
    Blue
}
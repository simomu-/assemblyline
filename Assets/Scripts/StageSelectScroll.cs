﻿using UnityEngine;
using UnityEngine.UI;

public class StageSelectScroll : MonoBehaviour {

	[SerializeField] ScrollRect m_ScrollRect;
	[SerializeField] RectTransform m_Content;
	[SerializeField] float m_MoveSpeed = 10f;
	int m_StageIndex = 0;
	int m_MaxStageCount;

	void Start () {
		m_MaxStageCount = m_Content.childCount - 1;
        m_StageIndex = 1;
        m_ScrollRect.horizontalNormalizedPosition = 1.0f;
    }

    void Update () {
		float position = (float)m_StageIndex / m_MaxStageCount;
		m_ScrollRect.horizontalNormalizedPosition = Mathf.Lerp (m_ScrollRect.horizontalNormalizedPosition, position, m_MoveSpeed * Time.deltaTime);

		//if (Input.GetKeyDown (KeyCode.RightArrow)) {
		//	MoveRight ();
		//}
		//if (Input.GetKeyDown (KeyCode.LeftArrow)) {
		//	MoveLeft ();
		//}
	}

	public void MoveRight(){
		if (m_StageIndex < m_MaxStageCount) {
			m_StageIndex++;
		}
	}

	public void MoveLeft(){
		if (m_StageIndex > 0) {
			m_StageIndex--;
		}
	}
}

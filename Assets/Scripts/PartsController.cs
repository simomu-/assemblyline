﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartsController : MonoBehaviour {

    [SerializeField] GameObject partsPrefab;

    Parts currentDragParts;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}
    
    void OnMouseEnter() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) {
            Parts p = hit.transform.root.GetComponent<Parts>();
            if(p != null) {
                currentDragParts = p;
            }
        }
    }

    void OnMouseDrag() {

        if(currentDragParts == null) {
            return;
        }

        Vector3 objectPointInScreen
            = Camera.main.WorldToScreenPoint(this.transform.position);

        Vector3 mousePointInScreen
            = new Vector3(Input.mousePosition.x,
                          Input.mousePosition.y,
                          objectPointInScreen.z);

        Vector3 mousePointInWorld = Camera.main.ScreenToWorldPoint(mousePointInScreen);
        mousePointInWorld.z = currentDragParts.transform.position.z;
        //this.transform.position = mousePointInWorld;
        currentDragParts.transform.position = mousePointInWorld;
    }

    void OnMouseExit() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) {
            CarTrailer t = hit.transform.root.GetComponent<CarTrailer>();
            if(t != null) {
                currentDragParts = null;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parts : MonoBehaviour {

    public static Parts currentDragParts;

    [SerializeField] CarType carType;
    public CarType GetCarType() {
        return carType;
    }

    Vector3 initPosition;
    bool isDrag = false;

    void Start() {
        initPosition = transform.position;
    }

    void Update() {

        if (Input.GetMouseButtonDown(0)) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit)) {
                if(hit.transform.gameObject == this.gameObject) {
                    isDrag = true;
                }
            }
        }

        if (isDrag) {
            Vector3 objectPointInScreen
                = Camera.main.WorldToScreenPoint(this.transform.position);

            Vector3 mousePointInScreen
                = new Vector3(Input.mousePosition.x,
                              Input.mousePosition.y,
                              objectPointInScreen.z);

            Vector3 mousePointInWorld = Camera.main.ScreenToWorldPoint(mousePointInScreen);
            mousePointInWorld.y = transform.position.y;
            transform.position = mousePointInWorld;
        }

        if (Input.GetMouseButtonUp(0)) {
            transform.position = initPosition;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit) && isDrag) {
                CarTrailer t = hit.transform.root.GetComponent<CarTrailer>();
                if(t != null) {
                    if(this.carType == CarType.None) {
                        t.CurrentCar.SetChassis();
                    }else {
                        t.CurrentCar.SetParts(carType);
                    }
                }
            }
            isDrag = false;

        }

    }

    //void OnMouseEnter() {
    //    Debug.Log("Drag Start");
    //    isDrag = true;
    //}

    //void OnMouseExit() {
    //    Debug.Log("Drag End");
    //    isDrag = false;
    //}

}

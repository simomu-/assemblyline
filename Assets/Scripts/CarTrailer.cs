﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarTrailer : MonoBehaviour {

    public static float speed;

    [SerializeField] Car car;

    Vector3 startPosition;
    Vector3 endPosition;
    bool isMove = true;
    float currentSpeed = 0;

    public Car CurrentCar {
        get { return car; }
    }

	// Use this for initialization
	void Start () {
        startPosition = new Vector3(0.0f + -3.5f * Random.Range(0, GameManager.Instance.LineCount), 0.2f, -10.0f);
        transform.position = startPosition;
        endPosition = startPosition + Vector3.forward * 20.0f;
        currentSpeed = speed + Random.Range(-0.25f, 0.25f);
        car.Init();
	}
	
	// Update is called once per frame
	void Update () {

        if (GameManager.Instance.isPlaying) {
            if (isMove) {
                transform.Translate(0.0f, 0.0f, currentSpeed * Time.deltaTime);
                if (transform.position.z >= endPosition.z) {
                    isMove = false;
                    car.IsComplete = true;
                    transform.position = endPosition;
                    car.transform.parent = null;
                    Destroy(gameObject, 1.0f);
                }
            } else {
                car.Judge();
            }
        }
       
    }
}

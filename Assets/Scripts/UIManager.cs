﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public static UIManager Instance;

    [SerializeField] CarUI UIPrefab;
    [SerializeField] Text carCountText;
    [SerializeField] Text missCountText;
    [SerializeField] RectTransform hiSpeedPanel;
    [SerializeField] StageSelectScroll uiControll;
    [SerializeField] Text resultText;
    [Space]
    [SerializeField] RectTransform gameOverPanel;
 
    List<CarUI> carUIs = new List<CarUI>();
    List<Car> cars = new List<Car>();
    int resultCount;

    void Awake() {
        Instance = this;
    }

    void Update() {
        carCountText.transform.localScale = Vector3.Lerp(carCountText.transform.localScale, Vector3.one, 10.0f * Time.deltaTime);
        missCountText.transform.localScale = Vector3.Lerp(missCountText.transform.localScale, Vector3.one, 10.0f * Time.deltaTime);
    }
    

    public void AddCarUI(Car car) {
        cars.Add(car);
        CarUI ui = Instantiate(UIPrefab) as CarUI;
        ui.transform.SetParent(transform, false);
        CarType type;
        ColorType colorType;
        car.CarInfo(out type, out colorType);
        ui.Init(type, colorType);
        ui.transform.SetAsFirstSibling();
        carUIs.Add(ui);
    }

    public void UpdateCarUI(Car car) {
        Vector3 pos = Camera.main.WorldToScreenPoint(car.transform.position);
        pos.z = 0;
        carUIs[cars.IndexOf(car)].transform.position = pos;
        carUIs[cars.IndexOf(car)].gameObject.SetActive(true);
    }

    public void SetBackgroundColor(Car car, Color c) {
        carUIs[cars.IndexOf(car)].SetBackgroundColor(c);
    }

    public void RemoveCarUI(Car car) {
        Destroy(carUIs[cars.IndexOf(car)].gameObject);
        carUIs.RemoveAt(cars.IndexOf(car));
        cars.Remove(car);
    }

    public void SetCountText(int count) {
        carCountText.text = count.ToString() + "台";
        carCountText.transform.localScale = Vector3.one * 2.0f;
    }

    public void SetMissCount(int count) {
        missCountText.text = count.ToString();
        missCountText.transform.localScale = Vector3.one * 2.0f;
    }

    public void SetHiSpeedPanel(bool enable) {
        if (enable) {
            StartCoroutine(HiSpeedPanelCoroutine());
        } else {
            StopAllCoroutines();
            hiSpeedPanel.gameObject.SetActive(false);
        }
    }

    IEnumerator HiSpeedPanelCoroutine() {
        while (true) {
            hiSpeedPanel.gameObject.SetActive(!hiSpeedPanel.gameObject.activeInHierarchy);
            yield return new WaitForSeconds(hiSpeedPanel.gameObject.activeInHierarchy ? 1.5f : 0.3f);
        }

    }

    public void SetGameOverPanel(bool enable) {
        gameOverPanel.gameObject.SetActive(enable); 
    }

    public void SetResult(int count) {
        resultCount = count;
        uiControll.MoveLeft();
        resultText.text = count.ToString() + "台の車を生産しました";
    }

    public void Title() {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Title");
    }

    public void SendRanking() {
        naichilab.RankingLoader.Instance.SendScoreAndShowRanking(resultCount);
    }

    public void SendTweet() {
        string text = string.Format("{0}台の車を生産しました\n#unity1week #unityroom #AssemblyLine\n", resultCount);
        text += "https://unityroom.com/games/assembly_line";
        string url = "http://twitter.com/intent/tweet?text=" + WWW.EscapeURL(text);
        if (Application.platform == RuntimePlatform.WebGLPlayer) {
            Application.ExternalEval("window.open(\"" + url + "\",\"_blank\")");
            return;
        }
        Application.OpenURL(url);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager Instance;

    [HideInInspector] public bool isPlaying = true;

    [SerializeField] float time;
    [SerializeField] int missCount;
    [SerializeField] CarTrailer carTrailerPrefab;
    [SerializeField] float waveInterval = 12.0f;
    [SerializeField] float normalTrailerCreateIntarval = 5.0f;
    [SerializeField] float minTrailerCreateInterval;
    [SerializeField] float normalSpeed = 2.5f;
    [SerializeField] float maxSpeed = 4.0f;

    AudioSource audioSource;
    float currentTime;
    float currentTrailerCreateInterval;
    float currentSpeed;
    bool isTimeCount = true;
    bool isHiSpeed = false;
    int lineCount = 3;
    int currentCarCount;

    public int LineCount {
        get { return lineCount; }
    }

    public int CarCount {
        get { return currentCarCount; }
    }

    void Awake() {
        Instance = this;
    }

    void Start() {
        currentTime = time;
        StartCoroutine(TrailerCreateCoroutine());
        StartCoroutine(TrailerWaveCoroutine());
        CarTrailer.speed = 2.5f;
        audioSource = GetComponent<AudioSource>();
    }

    void Update() {

        if (isPlaying) {
            currentTime += Time.deltaTime;
        }

    }

    IEnumerator TrailerCreateCoroutine() {
        while (true) {
            Instantiate(carTrailerPrefab);
            CarTrailer.speed = isHiSpeed ? maxSpeed : normalSpeed;
            yield return new WaitForSeconds(isHiSpeed ? minTrailerCreateInterval : normalTrailerCreateIntarval);
        }
    }

    IEnumerator TrailerWaveCoroutine() {
        yield return new WaitForSeconds(5.0f);
        while (true) {
            yield return new WaitForSeconds(waveInterval);
            isHiSpeed = !isHiSpeed;
            UIManager.Instance.SetHiSpeedPanel(isHiSpeed);
            if (isHiSpeed) {
                audioSource.Play();
            } else {
                audioSource.Stop();
            }
        }
    }

    public void AddCarCount() {
        currentCarCount++;
        //Debug.Log("add:" + currentCarCount);
        UIManager.Instance.SetCountText(currentCarCount);
    }

    public void CarPartsMiss() {
        missCount--;
        if(missCount <= 0) {
            missCount = 0;
            StopAllCoroutines();
            isPlaying = false;
            audioSource.Stop();
            StartCoroutine(GameOverCoroutine());
        }
        UIManager.Instance.SetMissCount(missCount);
    }

    IEnumerator GameOverCoroutine() {
        UIManager.Instance.SetGameOverPanel(true);
        yield return new WaitForSeconds(2.0f);
        while (true) {
            Vector3 pos = Camera.main.transform.position;
            pos.z = 30f;
            Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, pos, 5.0f * Time.deltaTime);
            UIManager.Instance.SetResult(currentCarCount);
            yield return null;
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarUI : MonoBehaviour {

    [SerializeField] RectTransform[] carTypeUIs;
    Image image;

    void Start() {
        image = GetComponent<Image>();
    }

    public void Init(CarType carType, ColorType colorType) {
        carTypeUIs[(int)carType].gameObject.SetActive(true);
        carTypeUIs[(int)carType].GetChild((int)colorType).gameObject.SetActive(true);
    }

    public void SetBackgroundColor(Color c) {
        image.color = c;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Title : MonoBehaviour {

    [SerializeField] RectTransform howToPlayPanel;
    Vector3 panelScale;

    void Start() {
        howToPlayPanel.localScale = Vector3.zero;
        panelScale = Vector3.zero;
    }

    void Update() {
        howToPlayPanel.localScale = Vector3.Lerp(howToPlayPanel.localScale, panelScale, 10.0f * Time.deltaTime);
    }

    public void OpenHowToPlay() {
        panelScale = Vector3.one;
    }

    public void GameStart() {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour {

    [SerializeField] CarType carType;
    [SerializeField] ColorType colorType;
    [Space]
    [SerializeField] GameObject chassisObjects;
    [SerializeField] GameObject[] carPartsObjects;
    [Space]
    [SerializeField] GameObject spark;
    [SerializeField] ParticleSystem smoke;
    [Space]
    [SerializeField] AudioClip setClip;
    [SerializeField] AudioClip sparkClip;
    [SerializeField] AudioClip smokeClip;
    [SerializeField] AudioClip collectClip;
    [SerializeField] AudioClip missClip;

    CarType cullentCarType;
    ColorType cullentColorType;

    AudioSource audioSource;
    bool isSetChassis = false;
    bool isSetParts = false;
    bool isSetColor = false;
    bool isComplete = false;
    bool isJudge = false;

    public bool IsComplete {
        get { return isComplete; }
        set { isComplete = value; }
    }

    public bool IsCollectParts {
        get { return (carType == cullentCarType && colorType == cullentColorType); }
    }

    public void CarInfo(out CarType carType, out ColorType colorType) {
        carType = this.carType;
        colorType = this.colorType;
    }

    //public void Init(CarType carType = CarType.None, ColorType colorType = ColorType.White) {
    //    this.carType = carType;
    //    this.colorType = colorType;
    //}

    void Start() {
        audioSource = GetComponent<AudioSource>();
    }

    public void Init() {
        carType = (CarType)System.Enum.ToObject(typeof(CarType), Random.Range(1, (int)CarType.Car4 + 1));
        colorType = (ColorType)System.Enum.ToObject(typeof(ColorType), Random.Range(0, (int)ColorType.Blue + 1));
        UIManager.Instance.AddCarUI(this);
    }

    void Update() {
        if (!isJudge) {
            UIManager.Instance.UpdateCarUI(this);
        }
    }

    public void SetChassis() {
        if (!isSetChassis && !isComplete) {
            chassisObjects.SetActive(true);
            isSetChassis = true;
            audioSource.PlayOneShot(setClip);
        }
    }

    public void SetParts(CarType type) {
        if (!isSetParts && !isComplete && isSetChassis) {
            cullentCarType = type;
            carPartsObjects[(int)cullentCarType].SetActive(true);
            isSetParts = true;
            spark.SetActive(true);
            audioSource.PlayOneShot(sparkClip);
        }
    }

    public void SetColor(ColorType color) {
        if (!isSetColor && !isComplete && isSetParts) {
            cullentColorType = color;
            carPartsObjects[(int)cullentCarType].transform.GetChild(0).gameObject.SetActive(false);
            carPartsObjects[(int)cullentCarType].transform.GetChild((int)cullentColorType).gameObject.SetActive(true);
            isSetColor = true;
            var m = smoke.main;
            switch (color) {
                case ColorType.Red:
                    m.startColor = new Color(121.0f / 255f, 17f / 255f, 17f / 255f);
                    break;
                case ColorType.Green:
                    m.startColor = new Color(17.0f / 255f, 141f / 255f, 17f / 255f);
                    break;
                case ColorType.Blue:
                    m.startColor = new Color(18.0f / 255f, 104f / 255f, 161f / 255f);
                    break;
            }
            smoke.gameObject.SetActive(true);
            audioSource.clip = smokeClip;
            audioSource.time = 0.5f;
            audioSource.Play();
        }

    }

    public void Judge() {
        if (!isJudge) {
            StartCoroutine(JudgeCoroutine());
            isJudge = true;
        }
    }

    IEnumerator JudgeCoroutine() {
        Color color;
        int count = 0;
        if (IsCollectParts) {
            color = Color.green;
            GameManager.Instance.AddCarCount();
            count = GameManager.Instance.CarCount;
            audioSource.PlayOneShot(collectClip);
        } else {
            color = Color.red;
            GameManager.Instance.CarPartsMiss();
            audioSource.PlayOneShot(missClip);
        }
        color.a = 148f / 255f;
        UIManager.Instance.SetBackgroundColor(this, color);
        yield return new WaitForSeconds(1.0f);
        UIManager.Instance.RemoveCarUI(this);
        float time = 0;
        while (time <= 2.0f) {
            time += Time.deltaTime;
            transform.Translate(0.0f, 0.0f, 3.0f * Time.deltaTime);
            yield return null;
        }
        if (IsCollectParts) {
            transform.position = new Vector3(10f - 3 *  ((count - 1)/ 8) , 0.0f, 40f - 3 * ( (count - 1) % 8));
            transform.eulerAngles = new Vector3(0.0f, -45f, 0.0f);
        } else {
            Destroy(gameObject);
        }

    }

    

}
